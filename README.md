# ngrok-oci

## Supported tags and respective `Dockerfile` links

* [`latest`](Dockerfile)
* [`armhf`](Dockerfile.armhf)

A [Docker][docker] image for [ngrok][ngrok] v3, introspected
tunnels to localhost.
It's based on the excellent work of [wernight/ngrok][wernight/ngrok]

## Features

* [x] **Small** Built using [Alpine][alpine]
* [x] **Simple** `http` or `https` exposes ngrok server `4040` port
* [x] **Secure** Runs as non-root user with a random UID `6737`

## Configuration

To see command-line options, run `docker run --rm rusian/ngrok ngrok --help`.

## Usage

Supposing you've an Apache or Nginx Docker container named
`web_service_container` listening on port 80:

```console
docker run --rm -it --link web_service_container rusian/ngrok ngrok http web_service_container:80
```

### Environment variables

*Please consider using directly the command-line arguments of Ngrok.*

If you use the default `CMD` (i.e. don't specify the ngrok command-line
but only `rusian/ngrok`),
then you can use instead envrionment variables magic below.

You simply have to link the Ngrok container to the application under the
`app` or `http` or `https` aliases, and all of the configuration will
be done for you by default.

Additionally, you can specify one of several environment variable (via `-e`)
to configure your Ngrok tunnel:

* `NGROK_AUTH` - Authentication key for your Ngrok account. This is needed
  for custom subdomains, custom domains, and HTTP authentication.
* `NGROK_SUBDOMAIN` - Name of the custom subdomain to use for your tunnel.
  You must also provide the authentication token.
* `NGROK_HOSTNAME` - Paying Ngrok customers can specify a custom domain.
  Only one subdomain or domain can be specified, with the domain taking
  priority.
* `NGROK_REMOTE_ADDR` - Name of the reserved TCP address to use for a TCP
  tunnel. You must also provide the authentication token.
* `NGROK_USERNAME` - Username to use for HTTP authentication on the tunnel.
  You must also specify an authentication token.
* `NGROK_PASSWORD` - Password to use for HTTP authentication on the tunnel.
  You must also specify an authentication token.
* `NGROK_PROTOCOL` - Can either be `HTTP`, `TLS` or `TCP`, and it defaults
  to `HTTP` if not specified. If set to `TCP`, Ngrok will allocate a port
  instead of a subdomain and proxy TCP requests directly to your application.
* `NGROK_PORT` - Port to expose (defaults to `80` for `HTTP` protocol, 443
  for `TLS` protocol). If the server is non-local, the hostname can also be
  specified, e.g. `192.168.0.102:80` or `dev.local:443`.
* `NGROK_REGION` - Location of the ngrok tunnel server; can be `us`
  (United States, default), `eu` (Europe), `ap` (Asia/Pacific) or `au` (Australia)
* `NGROK_LOOK_DOMAIN` - This is the domain name referred to by ngrok.
  (default: localhost).
* `NGROK_BINDTLS` - Toggle tunneling only HTTP or HTTPS traffic. When `true`,
  Ngrok only opens the HTTPS endpoint. When `false`, Ngrok only opens the
  HTTP endpoint
* `NGROK_HEADER` - Rewrites the Host header for incoming HTTP requests to
  determine which development site to display.
* `NGROK_DEBUG` - Toggle output of logs. When `true`, Ngrok will output
  logs to stdout.

#### Full example

We'll set up a simple example HTTP server in a docker container named `www`:

```console
docker run -v /usr/share/nginx/html --name www_data busybox true
```

```console
docker run --rm --volumes-from www_data busybox /bin/sh -c 'echo "<h1>Yo</h1>" > /usr/share/nginx/html/index.html'
```

```console
docker run -d -p 80 --volumes-from www_data --name www nginx
```

```bash
$ curl $(docker port www 80)
<h1>Yo</h1>
```

Now we'll link that HTTP server into an ngrok container to
expose it on the internet:

```console
docker run -d -p 4040 --link www:http --name www_ngrok rusian/ngrok
```

You can now access the [API][ngrok-api] to find the
assigned domain:

```console
curl $(docker port www_ngrok 4040)/api/tunnels
```

or access the web UI to see requests and responses:

```console
xdg-open http://$(docker port www_ngrok 4040)
```

### Helper

For common cases you may want to create an alias in your `~/.profile`
(or `~/.bashrc`, `~/.zshrc`, or equivalent):

```bash
function docker-ngrok() {
  docker run --rm -it --link "$1":http rusian/ngrok ngrok http http:80
}
# For ZSH with Oh-My-Zsh! and 'docker' plugin enabled, you can also enable auto-completion:
#compdef __docker_containers docker-ngrok
```

Then to run the simple example just do `docker-ngrok web_service_container`.

For non dockerized http targets consider this helper function:

```bash
function expose-ngrok() {
  docker run --rm --net=host -e NGROK_PORT="$1" rusian/ngrok
}
```

and then visit [localhost:4040](http://localhost:4040) for
receiving the links.

## Build image docker

```console
docker build --no-cache --tag <user>/ngrok -f Dockerfile .
```

Remember change `<user>` for real user.

## Feedbacks

Report issues/questions/feature requests on [GiTea Issues][issues].

Pull requests are very welcome!

## Contributions

Based in repo [docker-ngrok](https://github.com/wernight/docker-ngrok)

## License

This work is licensed under the [GNU GPLv3+](LICENSE)

[issues]: https://hgit.ga/jestupinanm/ngrok-oci/issues
[docker]: https://www.docker.io/
[ngrok]: https://ngrok.com/
[ngrok-api]: https://ngrok.com/docs#client-api
[alpine]: https://hub.docker.com/_/alpine
[wernight/ngrok]: https://hub.docker.com/r/wernight/ngrok
